resolvers mydns
    nameserver dns1 127.0.0.11:53

global
#   debug
    daemon
    #log                 127.0.0.1    local0
    log 127.0.0.1 local0 debug

  defaults
    mode http
    timeout         connect 25s
    timeout         client 25s
    timeout         client-fin 25s
    timeout         server 25s
    timeout         tunnel 7200s
    option          http-server-close
    option          httpclose
    log             global
    option          httplog
    backlog         4096
    maxconn         2000
    option          tcpka

  frontend  fe_secured
    bind            0.0.0.0:443 ssl crt /certs/chain.pem
    mode            http
    log             global
    option          httplog
    timeout         client   3600s
    backlog         4096
    maxconn         50000      
    option          httpclose
    option          tcpka
  
    # letsencrypt
    use_backend letsencrypt-backend if { path_beg /.well-known/acme-challenge/ }

    # Grafana backend
    use_backend be_isard-grafana if { path_beg /monitor } or { path_beg /monitor/ }

    # Loki backend
    use_backend be_isard-loki if { path_beg /loki }
    
    # Prometheus backend
    use_backend be_isard-prometheus if  { path_beg /prometheus } or { path_beg /prometheus/ }

  frontend prometheus
    bind *:9090
    http-request use-service prometheus-exporter if { path /metrics }
    stats enable
    stats uri /stats
    stats refresh 10s

  backend letsencrypt-backend
    server letsencrypt 127.0.0.1:8080

  backend be_isard-grafana
   http-request set-path %[path,regsub(^/monitor/?,/)]
   server isard-grafana isard-grafana:3000 maxconn 10 check port 3000 inter 5s rise 2 fall 3  resolvers mydns init-addr none

  backend be_isard-loki
    # Require a JWT token in the Authorization header
    http-request deny content-type 'text/html' string 'Missing Authorization HTTP header' unless { req.hdr(authorization) -m found }

    # get header part of the JWT
    http-request set-var(txn.alg) http_auth_bearer,jwt_header_query('$.alg')

    # get payload part of the JWT
    http-request set-var(txn.iss) http_auth_bearer,jwt_payload_query('$.iss')
    http-request set-var(txn.kid) http_auth_bearer,jwt_payload_query('$.kid')
    http-request set-var(txn.exp) http_auth_bearer,jwt_payload_query('$.exp','int')
    http-request set-var(txn.role) http_auth_bearer,jwt_payload_query('$.data.role_id')

    # Validate the JWT
    http-request deny content-type 'text/html' string 'Unsupported JWT signing algorithm'  unless { var(txn.alg) -m str HS256 }
    http-request deny content-type 'text/html' string 'Invalid JWT issuer'  unless { var(txn.iss) -m str isard-authentication }
    http-request deny content-type 'text/html' string 'Invalid JWT Key ID'  unless { var(txn.kid) -m str isardvdi }
    http-request deny content-type 'text/html' string 'Invalid JWT signature'  unless { http_auth_bearer,jwt_verify(txn.alg,"${API_ISARDVDI_SECRET}") -m int 1 }

    http-request set-var(txn.now) date()
    http-request deny content-type 'text/html' string 'JWT has expired' if { var(txn.exp),sub(txn.now) -m int lt 0 }

    # Deny requests that lack sufficient permissions
    http-request deny unless { var(txn.role) -m sub admin }

    http-request set-path %[path,regsub(^/loki/?,/)]
    server isard-loki isard-loki:3100 maxconn 10 check port 3100 inter 5s rise 2 fall 3  resolvers mydns init-addr none

  backend be_isard-prometheus
    # Require a JWT token in the Authorization header
    http-request deny content-type 'text/html' string 'Missing Authorization HTTP header' unless { req.hdr(authorization) -m found }

    # get header part of the JWT
    http-request set-var(txn.alg) http_auth_bearer,jwt_header_query('$.alg')

    # get payload part of the JWT
    http-request set-var(txn.iss) http_auth_bearer,jwt_payload_query('$.iss')
    http-request set-var(txn.kid) http_auth_bearer,jwt_payload_query('$.kid')
    http-request set-var(txn.exp) http_auth_bearer,jwt_payload_query('$.exp','int')
    http-request set-var(txn.role) http_auth_bearer,jwt_payload_query('$.data.role_id')

    # Validate the JWT
    http-request deny content-type 'text/html' string 'Unsupported JWT signing algorithm'  unless { var(txn.alg) -m str HS256 }
    http-request deny content-type 'text/html' string 'Invalid JWT issuer'  unless { var(txn.iss) -m str isard-authentication }
    http-request deny content-type 'text/html' string 'Invalid JWT Key ID'  unless { var(txn.kid) -m str isardvdi }
    http-request deny content-type 'text/html' string 'Invalid JWT signature'  unless { http_auth_bearer,jwt_verify(txn.alg,"${API_ISARDVDI_SECRET}") -m int 1 }

    http-request set-var(txn.now) date()
    http-request deny content-type 'text/html' string 'JWT has expired' if { var(txn.exp),sub(txn.now) -m int lt 0 }

    # Deny requests that lack sufficient permissions
    http-request deny unless { var(txn.role) -m sub admin }

    http-request set-path %[path,regsub(^/prometheus/?,/)]
    server isard-prometheus isard-prometheus:9090 maxconn 1000 check port 9090 inter 5s rise 2 fall 3  resolvers mydns init-addr none

  listen stats 
        bind                0.0.0.0:8888
        mode                http
        stats               enable
        option              httplog
        stats               show-legends
        stats               uri /haproxy
        stats               realm Haproxy\ Statistics
        stats               refresh 5s
        #stats               auth user:pass
        timeout             connect 5000ms
        timeout             client 50000ms
        timeout             server 50000ms
